<?php

namespace ATM\MailBundle\Queues\MailingOneTime;

use XLabs\RabbitMQBundle\RabbitMQ\Consumer as Parent_Consumer;
use ATM\MailBundle\Entity\DynMail;

class Consumer extends Parent_Consumer
{
    // set your custom consumer command name
    protected static $consumer = 'atm_mail_send_one_time:execute';

    // following function is required as it is
    protected function configure()
    {
        $this
            ->setName(self::$consumer)
        ;
    }

    // following function is required as it is
    public function getQueueName()
    {
        return Producer::getQueueName();
    }

    public function callback($msg)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $em = $container->get('doctrine.orm.default_entity_manager');
        $config = $container->getParameter('atm_mail_config');
        $dbal = $container->get('doctrine.dbal.default_connection');

        $arrBody = json_decode($msg->body,true);
        $mailingId = $arrBody['mailingId'];

        $qb = $em->createQueryBuilder();

        $qb
            ->select('partial mi.{id,data}')
            ->from('ATMMailBundle:MailItem','mi')
            ->join('mi.mailing','m','WITH',$qb->expr()->eq('m.id',$mailingId));

        $mailItems = $qb->getQuery()->getArrayResult();


        $mailClient = new DynMail($config['dyn_api_key']);
        $mailParameters = array();
        $mailParameters['from'] = array('name' => $config['from_name'],'address'=>$config['from_address']);
        $mailParameters['to'] = $config['checking_to_address'];
        $mailParameters['subject'] = 'Mailing one time start '. $config['site_domain'];
        $mailParameters['htmlBody'] = 'mailing start';
        $mailClient->send($mailParameters);

        foreach($mailItems as $mailItem){
            $data = json_decode($mailItem['data'],true);

            $address = $data['from']['address'];
            $mailName = $data['from']['name'];

            $mailParameters = array();
            $mailParameters['from'] = array('name' => $mailName,'address'=>$address);
            $mailParameters['to'] = $data['to'];
            $subject = $data['subject'];
            $mailParameters['subject'] = $subject;
            $mailParameters['textBody'] = $data['textBody'];
            $mailParameters['htmlBody'] = $data['htmlBody'];

            $mailParameters['xHeaders'] = array(
                array('name' => 'X-SiteId','value' => $data['X-SiteId']),
                array('name' => 'X-UserId','value' => $data['X-UserId']),
                array('name' => 'X-MailId','value' => $data['X-MailId'])
            );


            $mailClient->send($mailParameters);
            $dbal->delete('mail_item',array('id'=>$mailItem['id']));
        }


        $mailClient = new DynMail();
        $mailParameters = array();
        $mailParameters['from'] = array('name' => $config['from_name'],'address'=>$config['from_address']);
        $mailParameters['to'] = $config['checking_to_address'];
        $mailParameters['subject'] = 'Mailing one time finish '. $config['site_domain'];
        $mailParameters['htmlBody'] = 'mailing finish';
        $mailClient->send($mailParameters);
    }

}

