<?php

namespace ATM\MailBundle\Queues\SendImportedMail;

use XLabs\RabbitMQBundle\RabbitMQ\Consumer as Parent_Consumer;
use ATM\MailBundle\Entity\DynMail;


class Consumer extends Parent_Consumer
{
    private $router;

    // set your custom consumer command name
    protected static $consumer = 'atm_mail_send_imported:execute';

    // following function is required as it is
    protected function configure()
    {
        $this
            ->setName(self::$consumer)
        ;
    }

    public function __construct($router)
    {
        parent::__construct();
        $this->router = $router;
    }

    // following function is required as it is
    public function getQueueName()
    {
        return Producer::getQueueName();
    }

    public function callback($msg)
    {

        $data = json_decode($msg->body,true);

        $mailParametersConfirmation = array();
        $mailParametersConfirmation['from'] = array('name' => $data['from']['name'], 'address' => $data['from']['address']);
        $mailParametersConfirmation['to'] = 'alberto.tuzon@manicamedia.com';
        $mailParametersConfirmation['subject'] = 'Start Send imported mails';
        $mailParametersConfirmation['htmlBody'] = 'Start Send imported mails';
        $dynMail = new DynMail();
        $dynMail->send($mailParametersConfirmation);

        foreach($data['emails'] as $email) {
            $mailParameters = array();
            $mailParameters['from'] = array('name' => $data['from']['name'], 'address' => $data['from']['address']);
            $mailParameters['to'] = $email['email'];
            $mailParameters['subject'] = $data['subject'];

            $mailTokens = explode('@', $email['email']);
            $username = $mailTokens[0];
            $arrayWithTextToReplace = array(
                '%USERNAME%',
                '%EMAIL%',
                '%UNSUBSCRIBED_LINK%'
            );
            $arrayReplacements = array(
                $username,
                $email['email'],
                $this->router->generate('atm_mail_unsubscribe_mail', array('email' => $email['email']), 0)
            );

            $stringHtmlBodyReplaced = str_replace(
                $arrayWithTextToReplace,
                $arrayReplacements,
                $data['template']
            );


            $mailParameters['htmlBody'] = $stringHtmlBodyReplaced;

            $mailParameters['xHeaders'] = array(
                array('name' => 'X-SiteId', 'value' => $data['X-SiteId']),
                array('name' => 'X-UserId', 'value' => $email['id']),
                array('name' => 'X-MailId', 'value' => $data['X-MailId'])
            );

            $dyn = new DynMail();

            try {
                $dyn->send($mailParameters);
            } catch (\Exception $e) {
                dump($e->getMessage());
            }
        }

        $mailParametersConfirmation = array();
        $mailParametersConfirmation['from'] = array('name' => $data['from']['name'], 'address' => $data['from']['address']);
        $mailParametersConfirmation['to'] = 'alberto.tuzon@manicamedia.com';
        $mailParametersConfirmation['subject'] = 'Finish Send imported mails';
        $mailParametersConfirmation['htmlBody'] = 'Finish Send imported mails';
        $dynMail = new DynMail();
        $dynMail->send($mailParametersConfirmation);

    }

}

