<?php

namespace ATM\MailBundle\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;

class MailExtension extends \Twig_Extension{

    private $container;
    private $config;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $this->config = $this->container->getParameter('atm_mail_config');
    }

    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('getAtmMailVariables', array($this, 'getAtmMailVariables')),
        );
    }

    public function getAtmMailVariables($filePath){
        $templatePath = $this->container->get('kernel')->getRootDir().'/../web/'.$filePath;
        $html = file_get_contents($templatePath);

        $matches = array();
        preg_match_all('/%[\w]+%/', $html, $matches);

        return array_unique($matches[0]);
    }
}