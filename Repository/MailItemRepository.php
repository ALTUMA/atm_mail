<?php

namespace ATM\MailBundle\Repository;

use Doctrine\ORM\EntityRepository;
use \DateTime;

class MailItemRepository extends EntityRepository{

    public function getMailsToSend(){
        $qb = $this->getEntityManager()->createQueryBuilder();

        $currentDate = new DateTime();
        $minutes = (int)$currentDate->format('i');


        if($minutes < 29 ){
            $startMinutes = "00";
            $endMinutes = "29";
        }else{
            $startMinutes = "30";
            $endMinutes = "59";
        }

        $qb
            ->select('partial m.{id,userId,mailId,data,sendDate}')
            ->addSelect('partial ma.{id,mailingType,name}')
            ->from('ATMMailBundle:MailItem','m')
            ->join('m.mailing','ma')
            ->where(
                $qb->expr()->between(
                    'm.sendDate',
                    $qb->expr()->literal($currentDate->format('Y/m/d H').':'.$startMinutes.':00'),
                    $qb->expr()->literal($currentDate->format('Y/m/d H').':'.$endMinutes.':59')
                )
            );

        return $qb->getQuery()->getArrayResult();
    }
}