<?php

namespace ATM\MailBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UnsubscribedUserRepository extends EntityRepository{

    public function getIds(){
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select('partial u.{id,userId}')
            ->from('ATMMailBundle:UnsubscribedUser','u');

        $result = $qb->getQuery()->getArrayResult();

        $arrIds = array_map(function($v){
            return $v['userId'];
        }, $result);

        return $arrIds;
    }
}
