<?php

namespace ATM\MailBundle\Repository;

use Doctrine\ORM\EntityRepository;

class MailingRepository extends EntityRepository{

    public function getAll(){
        $dql = "SELECT partial m.{id,name,channelSubdomain,totalMails,bounced,spam,opened,clicked}
                FROM ATMMailBundle:Mailing m";

        return $this->getEntityManager()->createQuery($dql)->getArrayResult();
    }


    public function getOneById($mailingId){
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select('partial m.{id,name}')
            ->addSelect('COUNT(mi.id) as totalMails')
            ->from('ATMMailBundle:Mailing','m')
            ->leftJoin('m.mailItems','mi')
            ->where(
                $qb->expr()->eq('m.id',$mailingId)
            );

        $result = $qb->getQuery()->getArrayResult();
        return $result[0];
    }
}
