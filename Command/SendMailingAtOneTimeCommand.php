<?php

namespace ATM\MailBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ATM\MailBundle\Entity\DynMail;

class SendMailingAtOneTimeCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('send:mailing:one:time')
            ->addArgument('mailing_name',InputArgument::REQUIRED,'Mailing name');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = $this->getContainer()->getParameter('atm_mail_config');
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $dbal = $this->getContainer()->get('doctrine.dbal.default_connection');
        $mailingName = $input->getArgument('mailing_name');

        $qb = $em->createQueryBuilder();

        $qb
            ->select('partial mi.{id,data}')
            ->from('ATM\MailBundle:MailItem','mi')
            ->join('mi.mailing','m','WITH',$qb->expr()->eq('m.name',$qb->expr()->literal($mailingName)));

        $mailItems = $qb->getQuery()->getArrayResult();


        $mailClient = new DynMail($config['dyn_api_key']);
        $mailParameters = array();
        $mailParameters['from'] = array('name' => $config['from_name'],'address'=>$config['from_address']);
        $mailParameters['to'] = $config['checking_to_address'];
        $mailParameters['subject'] = 'Mailing one time start';
        $mailParameters['htmlBody'] = 'mailing start';
        $mailClient->send($mailParameters);

        foreach($mailItems as $mailItem){
            $data = json_decode($mailItem['data'],true);

            $address = $data['from']['address'];
            $mailName = $data['from']['name'];

            $mailParameters = array();
            $mailParameters['from'] = array('name' => $mailName,'address'=>$address);
            $mailParameters['to'] = $data['to'];
            $subject = $data['subject'];
            $mailParameters['subject'] = $subject;
            $mailParameters['textBody'] = $data['textBody'];
            $mailParameters['htmlBody'] = $data['htmlBody'];

            $mailParameters['xHeaders'] = array(
                array('name' => 'X-SiteId','value' => $data['X-SiteId']),
                array('name' => 'X-UserId','value' => $data['X-UserId']),
                array('name' => 'X-MailId','value' => $data['X-MailId'])
            );


            $mailClient->send($mailParameters);
            $dbal->delete('mail_item',array('id'=>$mailItem['id']));
        }


        $mailClient = new DynMail();
        $mailParameters = array();
        $mailParameters['from'] = array('name' => $config['from_name'],'address'=>$config['from_address']);
        $mailParameters['to'] = $config['checking_to_address'];
        $mailParameters['subject'] = 'Mailing one time finish';
        $mailParameters['htmlBody'] = 'mailing finish';
        $mailClient->send($mailParameters);
    }
}
