<?php

namespace ATM\MailBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ATM\MailBundle\Entity\DynMail;
use \DateTime;
use \Exception;

class SendCommand extends ContainerAwareCommand{

    protected function configure()
    {
        $this
            ->setName('atm:send:dyn:mails');
    }



    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);
        $config = $this->getContainer()->getParameter('atm_mail_config');
        $manager = $this->getContainer()->get('atm_mail_manager');
        $dbal = $this->getContainer()->get('doctrine.dbal.default_connection');

        $mailsToSend = $manager->getMailsToSend();

        $mailingIds = array();
        if(count($mailsToSend) > 0){
            $mailClient = new DynMail($config['dyn_api_key']);
            $changeSendDate = false;
            $progress = $this->getHelper('progress');
            $count = 0;
            $progress->start($output, count($mailsToSend));

            $mailParameters = array();
            $mailParameters['from'] = array('name' => $config['from_name'],'address'=>$config['from_address']);
            $mailParameters['to'] = $config['checking_to_address'];
            $mailParameters['subject'] = 'mailing start '.count($mailsToSend);
            $mailParameters['htmlBody'] = 'mailing start';
            $mailClient->send($mailParameters);
            foreach($mailsToSend as $i => $mail){
                $mailing = $mail['mailing'];
                $data = json_decode($mail['data'],true);
                if(!$changeSendDate){
                    if(isset($data['from']['address'])){
                        $address = $data['from']['address'];
                        $mailName = $data['from']['name'];
                    }else{
                        $mailTokens = explode('@',$data['from']);
                        $mailNameTokens = explode('.',$mailTokens[1]);
                        $mailName = $mailNameTokens[0];
                        $address = $data['from'];
                    }

                    $mailParameters = array();
                    $mailParameters['from'] = array('name' => $mailName,'address'=>$address);
                    $mailParameters['to'] = $data['to'];
                    $subject = (empty($data['subject'])) ? $mailing['mailingType'] : $data['subject'];
                    $mailParameters['subject'] = $subject;
                    $mailParameters['textBody'] = $data['textBody'];
                    $mailParameters['htmlBody'] = $data['htmlBody'];

                    $mailParameters['xHeaders'] = array(
                        array('name' => 'X-SiteId','value' => $data['X-SiteId']),
                        array('name' => 'X-UserId','value' => $data['X-UserId']),
                        array('name' => 'X-MailId','value' => $data['X-MailId'])
                    );

                    try {
                        $mailClient->send($mailParameters);
                        $dbal->delete('mail_item',array('id'=>$mail['id']));
                    } catch (Exception $e) {
                        $sendDate = $mail['sendDate'];
                        $dateTime = new DateTime($sendDate->format('Y-m-d H:i:s'));
                        $dateTime->modify('+30 minutes');
                        $dbal->update('mail_item',array('send_date'=>$dateTime->format('Y-m-d H:i:s')),array('id'=>$mail['id']));

                        $mailParameters = array();
                        $mailParameters['from'] = array('name' => $config['from_name'],'address'=>$config['from_address']);
                        $mailParameters['to'] = $config['to_addesss'];
                        $mailParameters['subject'] = 'mailing error '.$mailing['name'].' '.$data['to'];
                        $mailParameters['htmlBody'] = 'mailing error '.$mailing['name'].' '.$data['to'].' '.$e->getMessage();
                        $mailClient->send($mailParameters);
                    }

                }else{
                    $sendDate = $mail['sendDate'];
                    $dateTime = new DateTime($sendDate->format('Y-m-d H:i:s'));
                    $dateTime->modify('+30 minutes');
                    $dbal->update('mail_item',array('send_date'=>$dateTime->format('Y-m-d H:i:s')),array('id'=>$mail['id']));
                }

                if(!in_array($mailing['id'], $mailingIds)){
                    $mailingIds[] = $mailing['id'];
                }

                if($count == 500){
                    $changeSendDate = true;
                }
                $count++;
                $progress->setCurrent($count);
            }
            $progress->finish();

            $mailParameters = array();
            $mailParameters['from'] = array('name' => $config['from_name'],'address'=>$config['from_address']);
            $mailParameters['to'] = $config['checking_to_address'];
            $mailParameters['subject'] = 'mailing finish '.$count;
            $mailParameters['htmlBody'] = 'mailing finish';
            $mailClient->send($mailParameters);

        }
        $mailClient = new DynMail($config['dyn_api_key']);
        $mailParameters = array();
        $mailParameters['from'] = array('name' => $config['from_name'],'address'=>$config['from_address']);
        $mailParameters['to'] = $config['checking_to_address'];
        $mailParameters['subject'] = 'send mail command executed';
        $mailParameters['htmlBody'] = 'send mail command executed';
        $mailClient->send($mailParameters);

        $mailClient = new DynMail($config['dyn_api_key']);
        foreach($mailingIds as $mailingId){
            $mailing = $manager->getOneById($mailingId);
            if($mailing['totalMails'] == 0){
                $mailParameters = array();
                $mailParameters['from'] = array('name' => $config['from_name'],'address'=>$config['from_address']);
                $mailParameters['to'] = 'erwin.deboer@manicamedia.com';
                $mailParameters['subject'] = 'All mails for the mailing '.$mailing[0]['name'].' have been sent';
                $mailParameters['htmlBody'] = 'All mails for the mailing '.$mailing[0]['name'].' have been sent';
                $mailClient->send($mailParameters);
            }
        }
    }
}


