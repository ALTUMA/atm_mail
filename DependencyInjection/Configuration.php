<?php

namespace ATM\MailBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $treeBuilder
            ->root('atm_mail')
                ->children()
                    ->arrayNode('class')->isRequired()
                        ->children()
                            ->arrayNode('model')->isRequired()
                                ->children()
                                    ->scalarNode('user')->isRequired()->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                    ->arrayNode('user_types')
                        ->prototype('array')
                            ->children()
                                ->variableNode('roles')->end()
                                ->variableNode('not_roles')->end()
                                ->booleanNode('enabled')->end()
                                ->booleanNode('locked')->end()
                                ->booleanNode('affiliate')->end()
                                ->scalarNode('affiliate_field')->end()
                                ->scalarNode('expiresAt')->end()
                                ->booleanNode('approved')->end()
                                ->booleanNode('imported')->end()
                            ->end()
                        ->end()
                    ->end()
                    ->scalarNode('site_domain')->isRequired()->end()
                    ->scalarNode('templates_folder')->isRequired()->end()
                    ->scalarNode('from_address')->isRequired()->end()
                    ->scalarNode('from_name')->isRequired()->end()
                    ->scalarNode('checking_to_address')->isRequired()->end()
                    ->scalarNode('dyn_api_key')->isRequired()->end()
                    ->scalarNode('froala_key')->isRequired()->end()
                ->end();

        return $treeBuilder;
    }
}
