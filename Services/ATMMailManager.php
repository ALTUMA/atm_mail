<?php

namespace ATM\MailBundle\Services;

use Doctrine\ORM\EntityManager;
use \DateTime;

class ATMMailManager{

    private $em;
    private $config;

    public function __construct(EntityManager $em, $config){
        $this->em = $em;
        $this->config = $config;
    }


    public function totalUsersByType($userType,$getTotal = true,$excludedUsers = null){
        $qb = $this->em->createQueryBuilder();

        if(!isset($userType['imported'])){
            if($getTotal){
                $qb->select('COUNT(u.id) as totalUsers');
            }else{
                $qb->select('u');
            }

            $qb->from($this->config['class']['model']['user'],'u');

            $andX = $qb->expr()->andX();
            $addAndx = false;

            if(isset($userType['roles'])){
                $addAndx = true;
                foreach($userType['roles'] as $role){
                    $andX->add($qb->expr()->like('u.roles',$qb->expr()->literal('%'.$role.'%')));
                }
            }

            if(isset($userType['not_roles'])){
                $addAndx = true;
                foreach($userType['not_roles'] as $role){
                    $andX->add($qb->expr()->notLike('u.roles',$qb->expr()->literal('%'.$role.'%')));
                }
            }

            if(isset($userType['enabled'])){
                $addAndx = true;
                $andX->add($qb->expr()->eq('u.enabled',($userType['enabled']) ? 1 : 0));
            }

            if(isset($userType['locked'])){
                $addAndx = true;
                $andX->add($qb->expr()->eq('u.locked',($userType['locked']) ? 1 : 0));
            }

            if(isset($userType['expiresAt'])){
                $addAndx = true;
                $currentDate = new DateTime();
                $andX->add($qb->expr()->lt('u.expiresAt',$qb->expr()->literal($currentDate->format('Y-m-d'))));
            }

            if(isset($userType['approved'])){
                $andX->add($qb->expr()->eq('u.approved',($userType['approved']) ? 1 : 0));
            }

            if(isset($userType['affiliate'])){
                $qb->join('u.'.$userType['affiliate_field'],'af');
            }



            if($addAndx){
                $qb->where($andX);
            }


            if($excludedUsers){
                $qb->andWhere(
                    $qb->expr()->notIn('u.id',$excludedUsers)
                );
            }
        }else{
            if($getTotal){
                $qb->select('COUNT(i.id) as totalUsers');
            }else{
                $qb->select('i');
            }

            $qb
                ->from('ATMMailBundle:ImportedMails','i')
                ->where(
                    $qb->expr()->eq('i.unsubscribed',0)
                )
            ;
        }

        return $qb->getQuery()->getArrayResult();
    }

    public function getUnsubscribedUsersIds(){
        return $this->em->getRepository('ATMMailBundle:UnsubscribedUser')->getIds();
    }

    public function getUnsubscribedMailsIds(){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('partial i.{id,email}')
            ->from('ATMMailBundle:ImportedMails','i')
            ->where(
                $qb->expr()->eq('i.unsubscribed',1)
            )
        ;


        $result = $qb->getQuery()->getArrayResult();

        $arrIds = array_map(function($v){
            return $v['email'];
        }, $result);

        return $arrIds;
    }

    public function getMailsToSend(){
        $qb = $this->em->createQueryBuilder();

        $currentDate = new DateTime();
        $minutes = (int)$currentDate->format('i');


        if($minutes < 29 ){
            $startMinutes = "00";
            $endMinutes = "29";
        }else{
            $startMinutes = "30";
            $endMinutes = "59";
        }

        $qb
            ->select('partial m.{id,userId,mailId,data,sendDate}')
            ->addSelect('partial ma.{id,mailingType,name}')
            ->from('ATMMailBundle:MailItem','m')
            ->join('m.mailing','ma')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->between(
                        'm.sendDate',
                        $qb->expr()->literal($currentDate->format('Y/m/d H').':'.$startMinutes.':00'),
                        $qb->expr()->literal($currentDate->format('Y/m/d H').':'.$endMinutes.':59')
                    ),
                    $qb->expr()->eq('m.sendItOneTime',0)
                )
            );

        return $qb->getQuery()->getArrayResult();
    }

    public function getOneById($mailingId){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('partial m.{id,name}')
            ->addSelect('COUNT(mi.id) as totalMails')
            ->from('ATMMailBundle:Mailing','m')
            ->leftJoin('m.mailItems','mi')
            ->where(
                $qb->expr()->eq('m.id',$mailingId)
            );

        $result = $qb->getQuery()->getArrayResult();
        return $result[0];
    }
}