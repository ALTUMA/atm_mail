<?php

namespace ATM\MailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use ATM\MailBundle\Entity\Mailing;
use ATM\MailBundle\Entity\MailItem;
use ATM\MailBundle\Entity\MailTemplate;
use ATM\MailBundle\Form\MailTemplateType;
use \DateTime;

class MailController extends Controller
{
    public function indexAction(){
        $config = $this->getParameter('atm_mail_config');
        return $this->render('ATMMailBundle:Mail:index.html.twig',array(
            'user_types' => $config['user_types']
        ));
    }

    public function totalUsersAction($userType){
        $config = $this->getParameter('atm_mail_config');
        $mailManager = $this->get('atm_mail_manager');
        $totalUsers = $mailManager->totalUsersByType($config['user_types'][$userType]);
        return new Response($totalUsers[0]['totalUsers']);
    }

    public function generateMailingAction(){
        set_time_limit(0);
        $config = $this->getParameter('atm_mail_config');
        $mailManager = $this->get('atm_mail_manager');
        $dbal = $this->get('doctrine.dbal.default_connection');
        $em =  $this->getDoctrine()->getManager();
        $request = $this->get('request_stack')->getCurrentRequest();
        $allParameters = $request->request->all();

        $uploadedFilePath = $this->get('kernel')->getRootDir().'/../web/'.$config['templates_folder'];
        $uploadedFile = $request->files->get('mailTemplate');
        $randomEnding = abs(rand(1, 999999999999));
        $filename = str_replace('.html',$randomEnding.'.html',$uploadedFile->getClientOriginalName());
        $uploadedFile->move($uploadedFilePath, $filename);

        $userType = $allParameters['slcUserType'];

        $mailing = new Mailing($allParameters['mailingName']);
        $mailing->setUserGroup($userType);
        $em->persist($mailing);
        $em->flush();

        $mailUniqueId = md5($mailing->getName().uniqid());

        if(isset($config['user_types'][$userType]['imported'])){
            $users = $mailManager->totalUsersByType($config['user_types'][$userType],false);

            $data = array(
                'emails' => $users,
                'from' => array('address'=> $config['from_address'],'name'=>$config['from_name']),
                'subject' => $allParameters['subject'],
                'template' => file_get_contents($uploadedFilePath.'/'.$filename),
                'X-SiteId' => $config['site_domain'],
                'X-MailId' => $mailUniqueId
            );

            $producer = $this->get('atm_mail_send_imported_producer');
            $producer->process($data);

            $dbal->update('mailing',array('totalMails'=>count($users),'mailingType'=>$userType),array('id'=>$mailing->getId()));

            $session = $this->get('session');
            $session->getFlashBag()->add('atmmail_successMailList','Mailing list generated successfully');

            return new RedirectResponse($this->get('router')->generate('atm_mail_index'));
        }else{
            $unsubscribedUsers = $mailManager->getUnsubscribedUsersIds();
        }


        $users = $mailManager->totalUsersByType($config['user_types'][$userType],false,$unsubscribedUsers);


        $totalMails = 0;
        $subject = $allParameters['subject'];
        $router = $this->get('router');

        $sendItOneTime = false;
        $dateChoosen = $allParameters['chkDate'];
        if($dateChoosen == 'now'){
            $sendItOneTime = true;
        }

        foreach($users as $user){

            if(!empty($user['email'])){

                $totalMails++;
                $mailItem = new MailItem();
                $mailItem->setMailId($mailUniqueId);

                $htmlBodyFile = $uploadedFilePath.'/'.$filename;
                $stringHtmlBody = file_get_contents($htmlBodyFile);


                $arrayWithTextToReplace = array(
                    '%USERNAME%',
                    '%EMAIL%',
                    '%UNSUBSCRIBED_LINK%'
                );
                $arrayReplacements = array(
                    $user['username'],
                    $user['email'],
                    $router->generate('atm_mail_unsubscribe_user',array('userId'=>$user['id']),0)
                );

                $stringHtmlBodyReplaced = str_replace(
                    $arrayWithTextToReplace,
                    $arrayReplacements,
                    $stringHtmlBody
                );


                switch($dateChoosen){
                    case 'join_date':
                        $sendDate = $this->generateSendDate($user['joindate']);
                        break;
                    case 'now':
                        $sendDate = new DateTime();
                        $sendDate->modify('+2 hours');
                        break;
                    case 'custom_date':
                        $sendDate =  date_create_from_format('d-m-Y',$allParameters['customDate']);
                        break;
                }

                $data = array(
                    'from' => array('address'=> $config['from_address'],'name'=>$config['from_name']),
                    'to' => $user['email'],
                    'subject' => $subject,
                    'textBody' => '',
                    'htmlBody' => $stringHtmlBodyReplaced,
                    'X-SiteId' => $config['site_domain'],
                    'X-UserId' => $user['id'],
                    'X-MailId' => $mailUniqueId
                );


                $dbal->insert('mail_item',array(
                    'data' => json_encode($data),
                    'user_id' => $user['id'],
                    'send_date' => $sendDate->format('Y-m-d H:i:s'),
                    'mail_id' => $mailUniqueId,
                    'mailing_id' => $mailing->getId(),
                    'send_it_one_time' => 0
                ));
            }
        }

        unlink($uploadedFilePath.'/'.$filename);
        $dbal->update('mailing',array('totalMails'=>$totalMails,'mailingType'=>$userType),array('id'=>$mailing->getId()));

        $session = $this->get('session');
        $session->getFlashBag()->add('atmmail_successMailList','Mailing list generated successfully');


        if($sendItOneTime){
            $producer = $this->get('atm_mail_one_time_producer');
            $producer->process(array(
                'mailingId' => $mailing->getId(),
            ));
        }

        return new RedirectResponse($this->get('router')->generate('atm_mail_index'));
    }

    private function generateSendDate($loginDate){
        $loginDayOfWeek = $loginDate->format('N');
        $loginHourMinSec = $loginDate->format('H:i:s');

        $currentDate = new DateTime();
        $currentDayOfWeek = $currentDate->format('N');
        $currentHourMinSec = $currentDate->format('H:i:s');

        $loginTime = new DateTime($loginHourMinSec);
        $currentTime = new DateTime($currentHourMinSec);
        $diffTime = $currentTime->diff($loginTime);

        //if the login day and the actual day are the same and the time of login is previus than actual time
        $sendDate = new DateTime();
        $sendDate->setTime(0,0,0);
        if($currentDayOfWeek == $loginDayOfWeek && $diffTime->format('%R') == '+'){
            $sendDate->modify('+'.$loginDate->format('H').' hour');
            $sendDate->modify('+'.$loginDate->format('i').' minute');
        }else{
            //Calculate de day when the cronjob is going to send the mail
            if((int)$loginDayOfWeek < (int)$currentDayOfWeek ){
                $daysToAdd = 7-abs($currentDayOfWeek-$loginDayOfWeek);
                $sendDate->modify('+'.$daysToAdd.' day');
                $sendDate->modify('+'.$loginDate->format('H').' hour');
                $sendDate->modify('+'.$loginDate->format('i').' minute');
            }else{
                $daysToAdd = abs($loginDayOfWeek - $currentDayOfWeek);
                $sendDate->modify('+'.$daysToAdd.' day');
                $sendDate->modify('+'.$loginDate->format('H').' hour');
                $sendDate->modify('+'.$loginDate->format('i').' minute');
            }
        }

        return $sendDate;
    }

    public function unsubscribeUserAction($userId){
        $dbal = $this->get('doctrine.dbal.default_connection');

        $dbal->insert('unsubscribed_user',array('user_id'=>$userId));

        return new Response('You were unsubscribed successfully from the mailing');
    }

    public function unsubscribeAddressAction($email){
        $dbal = $this->get('doctrine.dbal.default_connection');

        $dbal->update('imported_mails',array('unsubscribed'=>true),array('email'=>$email));

        return new Response('You were unsubscribed successfully from the mailing');
    }


    public function showTemplatesAction(){

        $em = $this->getDoctrine()->getManager();

        $templates = $em->getRepository('ATMMailBundle:MailTemplate')->findAll();


        return $this->render('ATMMailBundle:Mail:mailTemplates.html.twig',array(
            'templates' => $templates
        ));
    }

    public function createTemplateAction(){
        $request = $this->get('request_stack')->getCurrentRequest();

        $mailTemplate = new MailTemplate();
        $form = $this->createForm(MailTemplateType::class, $mailTemplate);

        if($request->getMethod() == 'POST'){
            $em = $this->getDoctrine()->getManager();
            $form->handleRequest($request);
            $config = $this->getParameter('atm_mail_config');

            $templatesFolder = $config['templates_folder'];

            $uploadDir = $this->get('kernel')->getRootDir().'/../web/'.$templatesFolder.'/';

            if(!is_dir($uploadDir)){
                mkdir($uploadDir,0777);
            }

            $filename = $mailTemplate->getName().'.html';
            $file = $form['file']->getData();
            $file->move($uploadDir, $filename);
            $mailTemplate->setFile($templatesFolder.'/'.$filename);

            $em->persist($mailTemplate);
            $em->flush();

            return new RedirectResponse($this->get('router')->generate('atm_mail_templates'));
        }

        return $this->render('ATMMailBundle:Mail:createTemplate.html.twig',array(
            'form' => $form->createView()
        ));
    }

    public function editTemplateAction($templateId){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_mail_config');

        $mailTemplate = $em->getRepository('ATMMailBundle:MailTemplate')->findOneById($templateId);
        $templatePath = $this->get('kernel')->getRootDir().'/../web/'.$mailTemplate->getFile();


        $form = $this->createForm(MailTemplateType::class, $mailTemplate);

        $request = $this->get('request_stack')->getCurrentRequest();
        if($request->getMethod() == 'POST'){
            $filePath = $mailTemplate->getFile();
            $templateName = $mailTemplate->getName();
            $form->handleRequest($request);

            $file = $form['file']->getData();
            if($file){
                unlink($templatePath);
                $file->move(dirname($templatePath), basename($filePath));
            }else{
                $newHtml = $request->get('mailtemplate_html');
                if($newHtml){
                    file_put_contents($templatePath,$newHtml);
                }
            }


            $mailTemplate->setFile($filePath);
            $mailTemplate->setName($templateName);

            $em->persist($mailTemplate);
            $em->flush();

            return new RedirectResponse($this->get('router')->generate('atm_mail_templates'));
        }

        $htmlFile = file_get_contents($templatePath);

        return $this->render('ATMMailBundle:Mail:editTemplate.html.twig',array(
            'templateId' => $templateId,
            'htmlFile' => $htmlFile,
            'form' => $form->createView(),
            'froala_key' => $config['froala_key']
        ));
    }

    public function deleteTemplateAction($templateId){
        $em = $this->getDoctrine()->getManager();

        $mailTemplate = $em->getRepository('ATMMailBundle:MailTemplate')->findOneById($templateId);
        $templatePath = $this->get('kernel')->getRootDir().'/../web/'.$mailTemplate->getFile();

        unlink($templatePath);
        $em->remove($mailTemplate);
        $em->flush();

        return new RedirectResponse($this->get('router')->generate('atm_mail_templates'));
    }

    public function mailTemplatesVariablesAction(){
        $em = $this->getDoctrine()->getManager();

        $templates = $em->getRepository('ATMMailBundle:MailTemplate')->findAll();

        $matches = array();
        foreach($templates as $template){
            $arr = array();
            $templatePath = $this->container->get('kernel')->getRootDir().'/../web/'.$template->getFile();
            $html = file_get_contents($templatePath);
            preg_match_all('/%[\w]+%/', $html, $arr);

            $arr = array_unique($arr[0]);
            foreach($arr as $a){
                $matches[] = $a;
            }
        }

        return $this->render('ATMMailBundle:Mail:mailTemplatesVariables.html.twig',array(
            'variables' => array_unique($matches)
        ));
    }
}
