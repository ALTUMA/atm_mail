A Mail Manager System

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require atm/mailbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new ATM\MailBundle\ATMMailBundle(),
    ];
}
```

## Routing

Append to main routing file:

``` yml
# app/config/routing.yml
  
atm_mail:
    resource: "@ATMMailBundle/Resources/config/routing.yml"
    prefix:   /
```

You also have to put outside of your firewalls the following route for unsubscribing users
```yml
atm_mail_unsubscribe_user:
    path: /unsubscribe/{userId}
    defaults: { _controller: "ATMMailBundle:Mail:unsubscribeUser" }
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml
  
atm_mail:
    class:
        model:
            user: YourBundle\Entity\User
    # Domain of your site
    site_domain: 'atmbundles.com'
    # Folder name where the mail templates are gonna be stored
    templates_folder: 'mail_templates'
    # From address to use for sending mails
    from_address: 'example@example.com'
    # From name to use for sending mails
    from_name: 'EXAMPLE'
    # To Address to use for checking that the commands had been executed successfully 
    checking_to_address: 'alberto.tuzon@manicamedia.com'
    # Your DYN Key
    dyn_api_key: 'yourkeygoeshere'
    # User types to use for mailings
    user_types: ['girls','exmembers','active_users','all']
```
### User Types Conditions ###
You must define the following function in your User Repository for filtering the different types of users
you have for sending mails.

```php

namespace YourBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function getAtmMailingUserTypeWhereClause($qb,$andX,$userType){

        switch($userType){
            case 'usertype1':
                $andX->add($qb->expr()->like('u.roles',$qb->expr()->literal('%ROLE1%')));
                $andX->add($qb->expr()->eq('u.locked',0));
                $andX->add($qb->expr()->eq('u.enabled',1));
                break;
            case 'usertype2':
                $andX->add($qb->expr()->notLike('u.roles',$qb->expr()->literal('%ROLE2%')));
                $andX->add($qb->expr()->eq('u.locked',0));
                $andX->add($qb->expr()->eq('u.enabled',1));
                break;
            case 'usertype3':
                $andX->add($qb->expr()->notLike('u.roles',$qb->expr()->literal('%ROLE3%')));
                $andX->add($qb->expr()->lte('u.expiresAt','CURRENT_TIMESTAMP()'));
                break;
            case 'all':
                return null;
                break;
        }

        return $andX;
    }
}
```