<?php 

namespace ATM\MailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class MailTemplate{

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
    * @ORM\Column(name="file", type="string")
    */
    private $file;
    

    /**
    * @ORM\Column(name="name", type="string", length=255)
    */
    private $name;


    public function getId()
    {
        return $this->id;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file)
    {
        $this->file = $file;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $name = preg_replace("/[^A-Za-z0-9 ]/",'',$name);
        $name = str_replace(' ','',$name);
        $this->name = $name;
    }
}