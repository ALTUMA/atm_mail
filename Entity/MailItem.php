<?php

namespace ATM\MailBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ATM\MailBundle\Repository\MailItemRepository")
 */
class MailItem{

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\Column(name="user_id", type="integer")
    */
    private $userId;
    
    /**
    * @ORM\Column(name="mail_id", type="string")
    */
    private $mailId;

    /**
    * @ORM\Column(name="data", type="text")
    */
    private $data;

    /**
    * @ORM\Column(name="send_date", type="datetime")
    */
    private $sendDate;

    /**
    * @ORM\ManyToOne(targetEntity="Mailing",inversedBy="mailItems")
    */
    protected $mailing;

    /**
     * @ORM\Column(name="send_it_one_time", type="boolean", nullable=false)
     */
    private $sendItOneTime;


    public function getUserId()
    {
        return $this->userId;
    }
    
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    public function getData()
    {
        return $this->data;
    }
    
    public function setData($data)
    {
        $this->data = $data;
    }

    public function getSendDate()
    {
        return $this->sendDate;
    }
    
    public function setSendDate($sendDate)
    {
        $this->sendDate = $sendDate;
    }

    public function getMailId()
    {
        return $this->mailId;
    }
    
    public function setMailId($mailId)
    {
        $this->mailId = $mailId;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getMailing(){
        return $this->mailing;
    }

    public function getSendItOneTime()
    {
        return $this->sendItOneTime;
    }

    public function setSendItOneTime($sendItOneTime)
    {
        $this->sendItOneTime = $sendItOneTime;
    }
}