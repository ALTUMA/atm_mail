<?php

namespace ATM\MailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ATM\MailBundle\Repository\MailingRepository")
 */
class Mailing{

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\Column(name="name", type="text",length=255)
    */
    private $name;

    /**
    * @ORM\OneToMany(targetEntity="MailItem",mappedBy="mailing")
    */
    protected $mailItems;

    /**
    * @ORM\Column(name="totalMails", type="integer",nullable=true)
    */
    private $totalMails;

    /**
    * @ORM\Column(name="bounced", type="integer",nullable=true)
    */
    private $bounced;

    /**
    * @ORM\Column(name="spam", type="integer",nullable=true)
    */
    private $spam;

    /**
    * @ORM\Column(name="opened", type="integer",nullable=true)
    */
    private $opened;

    /**
    * @ORM\Column(name="clicked", type="integer",nullable=true)
    */
    private $clicked;

    /**
    * @ORM\Column(name="mailingType", type="text",length=255,nullable=true)
    */
    private $mailingType;

    /**
     * @ORM\Column(name="generated", type="boolean", nullable=false)
     */
    private $generated;

    /**
     * @ORM\Column(name="template_name", type="text",length=255, nullable=true)
     */
    private $templateName;

    /**
     * @ORM\Column(name="send_date", type="datetime",nullable=true)
     */
    private $sendDate;

    /**
     * @ORM\Column(name="subject", type="text",length=255, nullable=true)
     */
    private $subject;

    /**
     * @ORM\Column(name="user_group", type="text",length=255, nullable=true)
     */
    private $userGroup;

    public function __construct($name){
        $this->name = $name;
        $this->mailItems = new ArrayCollection();
        $this->bounced = 0;
        $this->spam = 0;
        $this->opened = 0;
        $this->clicked = 0;
        $this->generated = false;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getMailItems()
    {
        return $this->mailItems;
    }

    public function addMailItem($mailItem)
    {
        $this->mailItems[] = $mailItem;
    }

    public function getTotalMails()
    {
        return $this->totalMails;
    }

    public function setTotalMails($totalMails)
    {
        $this->totalMails = $totalMails;
    }

    public function getBounced()
    {
        return $this->bounced;
    }

    public function setBounced()
    {
        $this->bounced++;
    }

    public function getSpam()
    {
        return $this->spam;
    }

    public function setSpam()
    {
        $this->spam++;
    }

    public function getOpened()
    {
        return $this->opened;
    }

    public function setOpened()
    {
        $this->opened++;
    }

    public function getClicked()
    {
        return $this->clicked;
    }

    public function setClicked()
    {
        $this->clicked++;
    }
    
    public function getMailingType()
    {
        return $this->mailingType;
    }

    public function setMailingType($mailingType)
    {
        $this->mailingType = $mailingType;
    }

    public function getGenerated()
    {
        return $this->generated;
    }

    public function setGenerated($generated)
    {
        $this->generated = $generated;
    }

    public function getTemplateName()
    {
        return $this->templateName;
    }

    public function setTemplateName($templateName)
    {
        $this->templateName = $templateName;
    }

    public function getSendDate()
    {
        return $this->sendDate;
    }

    public function setSendDate($sendDate)
    {
        $this->sendDate = $sendDate;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    public function getUserGroup()
    {
        return $this->userGroup;
    }

    public function setUserGroup($userGroup)
    {
        $this->userGroup = $userGroup;
    }
}