<?php

namespace ATM\MailBundle\Entity;

use Dyn\MessageManagement;
use Dyn\MessageManagement\Mail;

class DynMail{
    private $manager;
    private $mail;
    private $apikey;

    public function __construct($apiKey = null){

        if($apiKey){
            $this->manager = new MessageManagement($apiKey);
        }else{
            $this->manager = new MessageManagement('694c3c7f686522c0fa0cf99398fbb1f4');
        }

        $this->mail = new Mail();
    }


    public function send($mailParameters){
        if(filter_var($mailParameters['to'], FILTER_VALIDATE_EMAIL)){
            $from = $mailParameters['from'];
            $to = $mailParameters['to'];
            $subject = $mailParameters['subject'];
            $textBody = (isset($mailParameters['textBody'])) ? $mailParameters['textBody'] : null;
            $htmlBody = $mailParameters['htmlBody'];
            $xHeaders = (isset($mailParameters['xHeaders'])) ? $mailParameters['xHeaders'] : array();

            $this->mail->setFrom($from['address'],$from['name'])
                       ->setTo($to)
                       ->setSubject($subject)
                       ->setHTMLBody($htmlBody);

            if($textBody != null){
                $this->mail->setTextBody($textBody);
            }

            foreach($xHeaders as $xHeader){
                $this->mail->setXHeader($xHeader['name'],$xHeader['value']);
            }

            $mailSent = $this->manager->send($this->mail);
            $this->mail = new Mail();

            return $mailSent;
        }else{
            return false;
        }
    }

}